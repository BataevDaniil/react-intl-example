const webpack = require('webpack');
const mode = require('./mode');

let config = {
	mode: mode,
	output: {
		publicPath: '/js/'
	},
	watch: (mode !== 'production'),
	watchOptions: {
		aggregateTimeout: 100,
		poll: 100
	},
	resolve: {
		modules: ['./node_modules'],
		extensions: ['.tsx', '.ts', '.js', 'jsx', '.json']
	},
	resolveLoader: {
		modules: ['./node_modules'],
		moduleExtensions: ['-loader'],
		extensions: ['.js']
	},
	module: {
		rules: [{
			// regex ts or tsx or js or jsx
			test: /\.[tj]sx?$/i,
			exclude:/(node_modules|bower_components)/,
			use: [
				'babel',
				'ts'
			]
		}]
	},
	plugins: [
	],
	// externals: {
	// 	'react': 'React',
	// 	'react-dom': 'ReactDOM',
	// }
};

if (mode === 'production')
	config.plugins.push(
		// new webpack.optimize.UglifyJsPlugin({
		// new webpack.optimization.minimize({
		// 	compress: {
		// 		warnings: false,
		// 		drop_console: true,
		// 		unsafe: true
		// 	}
		// }),
		// new webpack.optimize.SplitChunksPlugin({
		// 	name: 'common'
		// }),
	);
else {
	config.plugins.push(
		new webpack.NoEmitOnErrorsPlugin()
	);
	// config.devtool = 'source-map';
	config.devtool = 'eval';
}

module.exports = config;