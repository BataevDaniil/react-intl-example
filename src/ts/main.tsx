import * as React from 'react';
import {render} from 'react-dom';
import * as $ from 'jquery';

//dinamic downloat if window.Intl === false
// import * as Intl from 'intl'

import {IntlProvider,
        FormattedMessage,
        FormattedHTMLMessage,
        addLocaleData,
        FormattedDate,
        FormattedTime,
        FormattedRelative,
        FormattedNumber,
        defineMessages,
        injectIntl,
        intlShape,
} from 'react-intl';

import * as en from 'react-intl/locale-data/en';
import * as ru from 'react-intl/locale-data/ru';
import messages from './messages'

class App extends React.Component<object, object> {
	public state = {
		local: (navigator.languages && navigator.languages[0])
		       || navigator.language
		       || 'en-US'
	};

	constructor(props:object, public intl:any) {
		super(props);
		addLocaleData([...en, ...ru]);
	}

	public render() {
		return (
			<IntlProvider
				locale={this.state.local}
				messages={this.flattenMessages(messages[this.state.local])}
				textComponent='div'
			>

				<div>
					<h1 onClick={() => this.hendleChangeLocal('en-US')}>en-US</h1>
					<h1 onClick={() => this.hendleChangeLocal('ru')}>ru</h1>
					<hr/>
					<h1>Hello World!</h1>
					<hr/>
					<FormattedMessage
						id="app.hello"
						values={{name:'daniil'}}
						children={(txt:any) => (<h1>{txt}</h1>)}
					/>
					<hr/>
					<FormattedHTMLMessage
						id='app.HTMLinsert'
						values={{text: <b>'text'</b>}}
					/>
					<hr/>
					 <FormattedDate
						 value={new Date(12321321)}
						 year='numeric'
						 month='long'
						 day='2-digit'
					 />
					<hr/>
					<FormattedTime
						value={new Date(12321321)}
						year='numeric'
						month='long'
						day='2-digit'
					/>
					<hr/>
					<FormattedRelative
						value={new Date()}
						updateInterval={1000}
					/>
					<hr/>
					<FormattedRelative
						value={new Date(12321321)}
					/>
					<hr/>
					<FormattedRelative
						value={new Date(12321321)}
						style='numeric'
					/>
					<hr/>
					<FormattedNumber
						value={10}
						style='currency'
						currencyDisplay='symbol'
						currency={this.state.local === 'ru'? 'RUB': 'USD'}
					/>
					<hr/>
					<FormattedMessage
						id='app.plural'
						values={{insert: 'вставка', count: 10}}
					/>
					<hr/>
					<this.Textarea/>
					<hr/>
				</div>
			</IntlProvider>
		);
	}

	public Textarea:any = injectIntl(({intl}) =>
		<textarea placeholder={intl.formatMessage({id: 'app.placeholder' })}></textarea>
	);

	public hendleChangeLocal(str:string) {
		this.setState({
			local: str
		})
	};

	public flattenMessages(nestedMessages:any, prefix = ''):any {
		return Object.keys(nestedMessages).reduce((messages:any, key) => {
			let value = nestedMessages[key];
			let prefixedKey = prefix ? `${prefix}.${key}` : key;

			if (typeof value === 'string') {
				messages[prefixedKey] = value;
			} else {
				let obj =  this.flattenMessages(value, prefixedKey);
				let arr:string[] = [];
				arr = Object.keys(obj);
				for (let i = 0; i < arr.length; i++)
					messages[arr[i]] = obj[arr[i]];
			}

			return messages;
		}, {});
	}
}



$(() => {
	render(<App/>, $('#react').get(0));
});
