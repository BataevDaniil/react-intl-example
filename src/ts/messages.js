export default {
	'en-US': {
		app: {
			hello: 'Hello {name}',
			HTMLinsert: 'This insert HTML tag h2 {text}',
			plural: 'Text {insert} {count, plural, =0 {empty!} one {# one} other {# other}}',
			placeholder: 'placeholder'
		}
	},
	'ru': {
		app: {
			hello: 'Привет {name}!',
			HTMLinsert: 'Это вставлен <h2>HTML tag h2 {text}</h2>',
		plural: 'Текст {insert} {count, plural, =0 {пусто!} one {# один} other {# другое}}',
		placeholder: 'плэйсхолдер'
		}
	}
}